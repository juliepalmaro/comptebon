﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace le_compte_est_bon
{
    class Bag
    {
        protected List<int> numbers;

        public Bag()
        {
            this.numbers = new List<int>();
            for(int i = 1; i <= 10; i++)
            {
                this.numbers.Add(i);
                this.numbers.Add(i);
            }
            this.numbers.Add(25);
            this.numbers.Add(50);
            this.numbers.Add(75);
            this.numbers.Add(100);
            this.numbers.Add(25);
            this.numbers.Add(50);
            this.numbers.Add(75);
            this.numbers.Add(100);
        }

        public Bag(List<int> numbers)
        {
            this.numbers = new List<int>(numbers);
        }

        //Pick a blank
        public int Pick(Random rand)
        {
            int indice = rand.Next(0, this.numbers.Count);
            int pick = this.numbers[indice];
            this.numbers.RemoveAt(indice);
            return pick;
        }
    }
}

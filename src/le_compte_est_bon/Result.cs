﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace le_compte_est_bon
{
    class Result
    {
        protected int rightNumber;
        protected String solution;
        protected List<int> numbers;

        public Result()
        {
            this.numbers = new List<int>(6);
        }

        //Create the list with 6 numbers
        public void Generate(Bag bag)
        {
            Random rand = new Random();
            for(int i = 0; i < 6; i++)
            {
                this.numbers.Add(bag.Pick(rand));
            }

        }

        // generate the rightNumber with the 6 numbers
        public void Calculate()
        {
            List<int> list = new List<int>(this.numbers);
            Calcul calculate = new Calcul();
            int indiceA, indiceB, indiceOp = 0, numberA, numberB, result;
            String[] ops = { "+", "*", "-", "/" };
            Random rand = new Random();
            String calcul;

            do
            {
                result = -1;
                list = new List<int>(this.numbers);

                // randomly pick 1 number in the 6
                indiceA = rand.Next(list.Count);
                numberA = list[indiceA];
                list.RemoveAt(indiceA);
                calcul = "(" + numberA.ToString();

                while (list.Count != 0)
                {
                    result = -1;
                    indiceB = rand.Next(list.Count);
                    numberB = list[indiceB];
                    list.RemoveAt(indiceB);

                    //Choose the calculation operator + make calculation
                    while (result == -1)
                    {
                        indiceOp = rand.Next(0, 4);
                        result = calculate.CalculOperator(ops[indiceOp], numberA, numberB);
                    }
                    calcul += " " + ops[indiceOp] + " " + numberB.ToString() + ")";
                    numberA = result;
                }
            }
            while (result > 999 || result < 100);
            this.rightNumber = result;
            this.solution = calcul;
        }

        public int GetRightNumber()
        {
            return this.rightNumber;
        }

        public void SetRightNumber(int number)
        {
            if (number >= 100 && number <= 999)
                this.rightNumber = number;
        }

        public String GetSolution()
        {
            return this.solution;
        }

        public void SetSolution(String solution)
        {
            this.solution = solution;
        }

        public List<int> GetNumbers()
        {
            return this.numbers;
        }

        public void SetNumbers(List<int> numbers)
        {
            this.numbers = new List<int>(numbers);
        }

        public void AddNumbers(int number)
        {
            this.numbers.Add(number);
        }

        public void RemoveNumbers(int number)
        {
            this.numbers.Remove(number);
        }

        public void ShowNumbers()
        {
            for(int i = 0; i < this.numbers.Count(); i++)
            {
                Console.Write(this.numbers[i] + " ");
            }
            Console.WriteLine();
        }
    }
}

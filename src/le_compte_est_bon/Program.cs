﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace le_compte_est_bon
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean replay = false;
            Game game = new Game();

            do
            {
                replay = game.Gaming();
            }
            while (replay);
        }   
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace le_compte_est_bon
{
    class Calcul
    {
        public Boolean VerifyCalcul(string calcul, List<int> list)
        {
            Boolean error = false;
            String[] calculSplit = calcul.Split(' ');
            String op;
            int operation = -1;

            if (calculSplit.Length < 3)
                error = true;
            else
            {
                op = calculSplit[1];
                if (Int32.TryParse(calculSplit[0], out int a) && Int32.TryParse(calculSplit[2], out int b) && list.IndexOf(a) != -1 && list.IndexOf(b) != -1)
                    operation = this.CalculOperator(op, a, b);
                else
                    error = true;
            }
            if (operation == -1)
                return true;
            return error;
        }

        public bool VerifyDivision(int a, int b)
        {
            return (b != 0 && a % b == 0);
        }

        public bool VerifySubstraction(int a, int b)
        {
            return (a >= b);
        }

        public int CalculOperator(String op, int a, int b)
        {
            switch (op)
            {
                case "+":
                    return a + b;
                case "*":
                    return a * b;
                case "-":
                    if (VerifySubstraction(a, b))
                        return a - b;
                    break;
                case "/":
                    if (VerifyDivision(a, b))
                        return a / b;
                    break;
            }
            return -1;
        }

        public int Calculate(List<int> list, int a, int b, String op)
        {
            list.RemoveAt(list.IndexOf(a));
            list.RemoveAt(list.IndexOf(b));
            int operation = this.CalculOperator(op, a, b);
            list.Add(operation);
            return operation;
        }
    }
}

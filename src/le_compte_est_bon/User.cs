﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace le_compte_est_bon
{
    class User
    {
        protected List<int> numbers;
        protected String username;

        public User()
        {
            this.numbers = new List<int>(6);
        }

        public User(String username, List<int> numbers)
        {
            this.username = username;
            this.numbers = new List<int>(numbers);
        }

        public List<int> GetNumbers()
        {
            return this.numbers;
        }

        public void SetNumbers(List <int> list)
        {
            numbers = new List<int>(list);
        }

        public String GetUsername()
        {
            return this.username;
        }

        public void SetUsername(String username)
        {
            this.username = username;
        }

        public void ShowNumbers()
        {
            for (int i = 0; i < this.numbers.Count(); i++)
            {
                Console.Write(this.numbers[i] + " ");
            }
            Console.WriteLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace le_compte_est_bon
{
    class Game
    {
        public String Round(User user, Result result, Calcul calculate, ref int resultat)
        {
            String calcul;
            //Enter a valid calculation
            do
            {
                Console.WriteLine("Entrez votre calcul sous la forme a + b (avec les espaces)");
                calcul = Console.ReadLine();
            }
            while (calcul != "r" && calcul != "v" && calculate.VerifyCalcul(calcul, user.GetNumbers()));

            //If user enter a calculation, we display the result
            if (calcul != "v" && calcul != "r")
            {
                resultat = calculate.Calculate(user.GetNumbers(), Int32.Parse(calcul.Split(' ')[0]), Int32.Parse(calcul.Split(' ')[2]), calcul.Split(' ')[1]);
                Console.WriteLine(" = " + resultat + "\n");
                user.ShowNumbers();
            }

            //If player enter "r", so he wants to reset his calculations
            if (calcul == "r")
            {
                user.SetNumbers(result.GetNumbers());
                user.ShowNumbers();
            }
            return calcul;
        }

        public Boolean Gaming()
        {
            //calculations initialization 
            Calcul calculate = new Calcul();

            //creation of a bag with planks + creation of result with 6 blanks
            Bag bag = new Bag();
            Result result = new Result();
            result.Generate(bag);
            result.Calculate();

            //Creation of the user
            Console.WriteLine("Entrez votre pseudo");
            String username = Console.ReadLine();
            User user = new User(username, result.GetNumbers());

            //Display of the number to find and planks
            Console.WriteLine("Entrez v pour valider vos calculs");
            Console.WriteLine("Entrez r pour recommencer vos calculs");
            Console.WriteLine();
            Console.WriteLine("Nombre à trouver : " + result.GetRightNumber());
            Console.Write("Plaquettes : ");
            result.ShowNumbers();

            //Player round, while the player doesn't confirm his calculations
            String calcul;
            int resultat = 0;
            do
            {
                calcul = this.Round(user, result, calculate, ref resultat);
            }
            while (calcul != "v");

            //if the calculation result of the player is egal to the number asked, so he wins. Else he looses
            if (resultat == result.GetRightNumber())
                Console.WriteLine("Bravo vous avez gagné !");
            else
                Console.WriteLine("Vous avez perdu, voici la solutions : " + result.GetSolution());

            //Ask if the player wants to play again
            Console.WriteLine("Voulez-vous recommencer O/N ?");
            String play = Console.ReadLine();
            if (play == "O")
                return true;
            else
                return false;
        }
    }
}
